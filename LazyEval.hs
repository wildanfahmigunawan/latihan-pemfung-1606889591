--Buatlah fungsi divisor yang menerima sebuah bilangan bulat ​n​ dan mengembalikanlist bilangan bulat positif yang membagi habis ​n

myDivisor n = [x | x <- [1..n] , n `mod` x == 0]

--Buatlah definisi ​quick sort​ menggunakan list comprehension.

quicksort [] = []
quicksort (x:xs) = quicksort [y| y <- xs , y <= x] ++ [x] ++ quicksort [y| y <- xs, y>x]

main = print(quicksort [12,43,85,91,32,11,9,53,21,58])