--List Comprehension to HOF

myMap f [] = []
myMap f (x:xs) = f x : myMap f xs

myFilter p [] = []
myFilter p (x:xs)
    | p x = x : myFilter p xs
    | otherwise = myFilter p xs

--[x+1 | x <- xs]

mySum [] = []
mySum (x:xs) = x+1 : mySum xs
mySumm x = x+1

rSum xs =  myMap mySumm xs

--[x+y | x <- xs, y <- ys]

twoSum xs ys = concat (myMap (\x -> myMap (\y -> x+y) ys) xs)

--[x+2 | x <- xs, x > 3]

moreThanTree x = x > 3

plusTwo x = x+2
pplusTwo xs = myMap plusTwo (myFilter moreThanTree xs) 

--HOF to List Comprehension

--map (+3) xs

plusThree xs = [x+3 | x <- xs]

--filter (>7) xs

moreThanSeven xs = [x | x <- xs, x >7]

--concat (map (\x -> map (\y -> (x,y)) ys) xs)

makeTuple xs ys = [(x,y)| x <- xs, y <- ys]

--filter (>3) (map (\(x,y) -> x+y) xys)

plusMoreThanTree xys = [x+y | (x,y) <- xys , x+y > 3]

main =  print(plusMoreThanTree [(1,3),(2,4)])